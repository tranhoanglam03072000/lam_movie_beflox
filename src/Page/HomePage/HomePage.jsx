import React from "react";
import HeaderPage from "../../components/Header/HeaderPage";
import BannerHomePage from "./Banner/BannerHomePage";
import MovieList from "./MovieList/MovieList";
import MovieTabs from "./MovieTab/MovieTabs";
import NewsMovie from "./News/NewsMovie";
import SpeedBooking from "./SpeedBookingMovie/SpeedBooking";
import { Dropdown, Menu } from "antd";
const menu = (
  <Menu
    items={[
      {
        key: "1",
        label: (
          <a
            target="_blank"
            rel="noopener noreferrer"
            href="https://www.antgroup.com"
          >
            1st menu item
          </a>
        ),
      },
      {
        key: "2",
        label: (
          <a
            target="_blank"
            rel="noopener noreferrer"
            href="https://www.aliyun.com"
          >
            2nd menu item (disabled)
          </a>
        ),
        disabled: true,
      },
      {
        key: "3",
        label: (
          <a
            target="_blank"
            rel="noopener noreferrer"
            href="https://www.luohanacademy.com"
          >
            3rd menu item (disabled)
          </a>
        ),
        disabled: true,
      },
      {
        key: "4",
        danger: true,
        label: "a danger item",
      },
    ]}
  />
);
export default function HomePage() {
  return (
    <div>
      <HeaderPage />
      <BannerHomePage />
      <SpeedBooking />
      <MovieList />
      <h1 className="xl:text-4xl md:text-4xl text-3xl text-center text-purple-700 font-bold mb-8">
        Lịch chiếu phim
      </h1>
      <MovieTabs />
      <NewsMovie />
      <Dropdown overlay={menu} trigger={["hover", "click"]}>
        <a>Hover me</a>
      </Dropdown>
    </div>
  );
}
