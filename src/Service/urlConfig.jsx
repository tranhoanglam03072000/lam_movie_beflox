import axios from 'axios';
import { store } from '..';
import { BAT_LOADING, TAT_LOADING } from '../redux/constant/LoadingConstant';
import { userLocal } from './localService';

export const BASE_URL = 'https://movienew.cybersoft.edu.vn';
export const TOKEN_CYBER =
  'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0ZW5Mb3AiOiJCb290Y2FtcCAzM0UiLCJIZXRIYW5TdHJpbmciOiIyNi8wNC8yMDI5IiwiSGV0SGFuVGltZSI6IjE2ODI0NjcyMDAwMDAiLCJuYmYiOjE3NTQzNjIwMDAsImV4cCI6MTY4MjYxNDgwMH0.0NjPvVOC1kHOsb5GFYmxL4IC7-fBrfOTsk52f-srpK8';
export const configHeader = () => {
  return {
    TokenCybersoft: TOKEN_CYBER,
    Authorization: 'Bearer ' + userLocal.get()?.accessToken,
  };
};
export const MA_NHOM = 'GP09';
export const https = axios.create({
  baseURL: BASE_URL,
  headers: configHeader(),
});
// Add a request interceptor
https.interceptors.request.use(
  (config) => {
    // Do something before request is sent
    let newConfig = {
      ...config,
      headers: {
        ...config.headers,
        Authorization: 'Bearer ' + userLocal.get()?.accessToken,
      },
    };
    store.dispatch({
      type: BAT_LOADING,
    });
    return newConfig;
  },
  (error) => {
    // Do something with request error
    return Promise.reject(error);
  },
);

// Add a response interceptor
https.interceptors.response.use(
  function (response) {
    setTimeout(() => {
      store.dispatch({
        type: TAT_LOADING,
      });
    }, 700);
    // Any status code that lie within the range of 2xx cause this function to trigger
    // Do something with response data
    return response;
  },
  function (error) {
    store.dispatch({
      type: TAT_LOADING,
    });

    // Any status codes that falls outside the range of 2xx cause this function to trigger
    // Do something with response error
    return Promise.reject(error);
  },
);
